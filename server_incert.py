from universalcheck import novell_library

LAGS = "10.56.80.157:443,tqidnproagqc01 10.56.80.160:443,tqidnproagqc02 10.56.80.163:443,tqidnproagqc03 10.56.80.166:443,tqidnproagqc04 10.56.80.109:443,tqidnproagmc01 10.56.80.112:443,tqidnproagmc02 10.56.80.115:443,tqidnproagmc03 10.56.80.118:443,tqidnproagmc04 10.56.80.133:443,tqidnproagnc01 10.56.80.136:443,tqidnproagnc02 10.56.80.139:443,tqidnproagnc03 10.56.80.142:443,tqidnproagnc04 10.56.80.145:443,tqidnproagec01 10.56.80.148:443,tqidnproagec02 10.56.80.151:443,tqidnproagec03 10.56.80.154:443,tqidnproagec04 10.56.80.121:443,tqidnproaggc01 10.56.80.124:443,tqidnproaggc02 10.56.80.127:443,tqidnproaggc03 10.56.80.130:443,tqidnproaggc04 10.56.80.97:443,tqidnproaghc01 10.56.80.100:443,tqidnproaghc02 10.56.80.73:443,tqidnproaglc01 10.56.80.76:443,tqidnproaglc02 10.56.80.79:443,tqidnproaglc03 10.56.80.82:443,tqidnproaglc04 10.56.80.55:443,tqidnproagbc01 10.56.80.58:443,tqidnproagbc02 10.56.80.61:443,tqidnproagbc03 10.56.80.64:443,tqidnproagbc04 10.56.80.103:443,tqidnproagvc01 10.56.80.106:443,tqidnproagvc02 10.56.80.13:443,tqidnproagsc01 10.56.80.16:443,tqidnproagsc02 10.56.80.19:443,tqidnproagsc03 10.56.80.22:443,tqidnproagsc04 10.56.80.25:443,tqidnproagsc05 10.56.80.28:443,tqidnproagsc06"
LAGE = "10.56.53.72:80,tqidnprolage01 10.56.53.73:80,tqidnprolage02 10.56.53.74:80,tqidnprolage03 10.56.53.75:80,tqidnprolage04 10.56.61.40:80,tqidnprolage05 10.56.61.41:80,tqidnprolage06"
aux = []
aux2 = []
hostname = []
for datas in LAGE.split(" "):
    hostname.append(datas.split(",")[1])
    aux.append(datas.split(",")[0])

for i in aux:
    aux2.append(i.split(":"))
d = []
for l in aux2:
    for h in hostname:
        d.append((h, l[0], l[1]))
obj = novell_library.DataBase("/tmp/log", "universalcheck/servers.db")
TABLE = "Servers"
statements = '''(Hostname, Ip, Port)'''
provisory = '''(?,?,?)'''

for g in d:
    obj.insertion(TABLE, statements, provisory, g)
