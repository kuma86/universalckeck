from distutils.core import setup

setup(
    name='universalcheck',
    version='1',
    packages=['universalcheck'],
    url='https://www.novell.com',
    license='BSD',
    author='Jesus Becerril Navarrete',
    author_email='JBecerril@novell.com',
    description='Aplicacion que genera las vitacoras para  el reporte de SLAS del SAT'
)
