#!/bin/bash
#Check Universal de Disponibilidad SLAs
#jsusunaga@novell.com
#Con funcionalidad de server

FECHA=`date +%d-%m-%Y`
HORA=`date +%H:%M`
HOST=`hostname`
LOG=/var/log/availability/`hostname`.availability.`date "+%Y%m%d-%H"`
LAGS="10.56.80.157:443,tqidnproagqc01 10.56.80.160:443,tqidnproagqc02 10.56.80.163:443,tqidnproagqc03 10.56.80.166:443,tqidnproagqc04 10.56.80.109:443,tqidnproagmc01 10.56.80.112:443,tqidnproagmc02 10.56.80.115:443,tqidnproagmc03 10.56.80.118:443,tqidnproagmc04 10.56.80.133:443,tqidnproagnc01 10.56.80.136:443,tqidnproagnc02 10.56.80.139:443,tqidnproagnc03 10.56.80.142:443,tqidnproagnc04 10.56.80.145:443,tqidnproagec01 10.56.80.148:443,tqidnproagec02 10.56.80.151:443,tqidnproagec03 10.56.80.154:443,tqidnproagec04 10.56.80.121:443,tqidnproaggc01 10.56.80.124:443,tqidnproaggc02 10.56.80.127:443,tqidnproaggc03 10.56.80.130:443,tqidnproaggc04 10.56.80.97:443,tqidnproaghc01 10.56.80.100:443,tqidnproaghc02 10.56.80.73:443,tqidnproaglc01 10.56.80.76:443,tqidnproaglc02 10.56.80.79:443,tqidnproaglc03 10.56.80.82:443,tqidnproaglc04 10.56.80.55:443,tqidnproagbc01 10.56.80.58:443,tqidnproagbc02 10.56.80.61:443,tqidnproagbc03 10.56.80.64:443,tqidnproagbc04 10.56.80.103:443,tqidnproagvc01 10.56.80.106:443,tqidnproagvc02 10.56.80.13:443,tqidnproagsc01 10.56.80.16:443,tqidnproagsc02 10.56.80.19:443,tqidnproagsc03 10.56.80.22:443,tqidnproagsc04 10.56.80.25:443,tqidnproagsc05 10.56.80.28:443,tqidnproagsc06"
LAGE="10.56.53.72:80,tqidnprolage01 10.56.53.73:80,tqidnprolage02 10.56.53.74:80,tqidnprolage03 10.56.53.75:80,tqidnprolage04 10.56.61.40:80,tqidnprolage05 10.56.61.41:80,tqidnprolage06"

#eDirectory
function edir389 {
PORT389=`ldapsearch -LLL -x -h localhost -p 389 -D cn=nts_proxy,ou=servicios,o=sat -w 4gr33 -b cn=nts_proxy,ou=servicios,o=sat cn | grep ^cn | awk '{print $NF}'`
if [ -n ${PORT389} ] ; then
        PORT389COMENT=`echo "UP"`
        else            
        PORT389COMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${PORT389COMENT}" >> ${LOG}
}

#LAGs 
function lagheartbeat {
for j in `echo ${LAGS}`
do
   IP=`echo ${j} | cut -d"," -f1`
   LAG=`echo ${j} | cut -d"," -f2`
   if [[ ${HOST} == ${LAG} ]] ; then 
   HEARTBEAT=`curl -o /dev/null --connect-timeout 4 -s -w '%{http_code},%{time_total}' -k https://${IP}/nesp/app/heartbeat | grep "200,"`
      if [[ -n ${HEARTBEAT} ]] ; then
           HBEATCOMENT=`echo "UP"`
           else
           HBEATCOMENT=`echo "DOWN"`
      fi
   fi
done
echo "${HOST}|${FECHA}|${HORA}|${HBEATCOMENT}" >> ${LOG}
}


function lagheartbeatemp {
for j in `echo ${LAGE}`
do
   IP=`echo ${j} | cut -d"," -f1`
   LAG=`echo ${j} | cut -d"," -f2`
   if [[ ${HOST} == ${LAG} ]] ; then
   HEARTBEAT=`curl -o /dev/null --connect-timeout 4 -s -w '%{http_code},%{time_total}' http://${IP}/nesp/app/heartbeat | grep "200,"`
      if [[ -n ${HEARTBEAT} ]] ; then
           HBEATCOMENT=`echo "UP"`
           else
           HBEATCOMENT=`echo "DOWN"`
      fi
   fi
done
echo "${HOST}|${FECHA}|${HORA}|${HBEATCOMENT}" >> ${LOG}
}



#IDP
function idpheartbeat {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
HEARTBEAT=`curl -o /dev/null --connect-timeout 4 -s -w '%{http_code}, %{time_total}' \\n -k https://${IFACE}:8443/nidp/idff/heartbeat | grep "200,"`
if [[ -n ${HEARTBEAT} ]] ; then
        HBEATCOMENT=`echo "UP"`
        else
        HBEATCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${HBEATCOMENT}" >> ${LOG}
}

#Consolas - iManager
function tomcat {
TOMCAT=`ps U novlwww | grep tomcat | awk '{print $1}'`
if [ -n ${TOMCAT} ] ; then
        TOMCATCOMENT=`echo "UP"`
        else
        TOMCATCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${TOMCATCOMENT}" >> ${LOG}
}

#Auth SATCServices - SATAuthenticator
function indexauth {
satcserv
satauth
echo "${HOST}|${FECHA}|${HORA}|${SATCSERVCOMENT}|${SATAUTHCOMENT}" >> ${LOG}
}

function satcserv {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
SATC8080=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:8080/SATCServices/AuthenticationService?wsdl | grep "200,"`
if [[ -n ${SATC8080} ]] ; then
        SATCSERVCOMENT=`echo "UP"`
        else
        SATCSERVCOMENT=`echo "DOWN"`
fi
}

#Auth SATAuthenticator
function satauth {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
SATA8080=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:8080/SATAuthenticator/AuthLogin/showLogin.action | grep "200,"`
if [[ -n ${SATA8080} ]] ; then
        SATAUTHCOMENT=`echo "UP"`
        else
        SATAUTHCOMENT=`echo "DOWN"`
fi
}

#Contraseña
function indexciec {
ciecinter
ciecintra
echo "${HOST}|${FECHA}|${HORA}|${INTERCOMENT}|${INTRACOMENT}" >> ${LOG}
}

function ciecinter {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
CURL8080=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:8080/CIECInternet/ | grep "200,"`
if [[ -n ${CURL8080} ]] ; then
        INTERCOMENT=`echo "UP"`
        else
        INTERCOMENT=`echo "DOWN"`
fi
}

function ciecintra {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
CURL8080=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:8080/CIECIntranet/ | grep "200,"`
if [[ -n ${CURL8080} ]] ; then
        INTRACOMENT=`echo "UP"`
        else
        INTRACOMENT=`echo "DOWN"`
fi
}

#WSAU WSIdentity
function wsau {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
CURL80=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:80/WSIdentity/WSAA?wsdl | grep "200,"`
if [[ -n ${CURL80} ]] ; then
        CURL80COMENT=`echo "UP"`
        else
        CURL80COMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${CURL80COMENT}" >> ${LOG}
}

#WSFE WSIdentity
function wsfe {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
CURL8080=`curl -o /dev/null --connect-timeout 4 -s -w 'HTTP %{http_code}, %{time_total}s' \\n http://${IFACE}:8080/WSIdentity/WSAA?wsdl | grep "200,"`
if [[ -n ${CURL8080} ]] ; then
        CURL8080COMENT=`echo "UP"`
        else
        CURL8080COMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${CURL8080COMENT}" >> ${LOG}
}

#SuperLumin
function superlumin {
IFACE=`ip addr sh eth1 | grep inet | grep eth1$ | grep -v secondary | awk '{print $2}' | cut -d"/" -f1`
PROXY=`netcat -nvz ${IFACE} 8080 2>&1 | awk '{print $NF}'`
if [[ ${PROXY} = "open" ]] ; then
        PROXYCOMENT=`echo "UP"`
        else
        PROXYCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${PROXYCOMENT}" >> ${LOG}
}

#Nagios
function nagios {
NAGIOS=`ps -fea | grep "/usr/sbin/nagios -d /etc/nagios/nagios.cfg" | head -1 | awk '{print $2}'`
if [ -n ${NAGIOS} ] ; then
        NAGIOSCOMENT=`echo "UP"`
        else
        NAGIOSCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${NAGIOSCOMENT}" >> ${LOG}
}


#Nagios
function noc {
NOC=`ps U nocsat | grep mosdaemon | awk '{print $1}'`
if [ -n ${NOC} ] ; then
        NOCCOMENT=`echo "UP"`
        else
        NOCCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${NOCCOMENT}" >> ${LOG}
}

#Server
function server {
SERVER=`uptime | grep up | awk '{print $2}'`
if [ -n ${SERVER} ] ; then
        SERVERCOMENT=`echo "UP"`
        else
        SERVERCOMENT=`echo "DOWN"`
fi
echo "${HOST}|${FECHA}|${HORA}|${SERVERCOMENT}" >> ${LOG}
}


case "$1" in
        edir)
	        edir389
        ;;
        lag)
		lagheartbeat
        ;;
	lage)
		lagheartbeatemp
	;;
        idp)
		idpheartbeat
        ;;
        consola)
		tomcat
        ;;
        auth)
		indexauth
        ;;
        ciec)
		indexciec
        ;;
        wsau)
		wsau
        ;;
        wsfe)
		wsfe
        ;;
        uapp)
        ;;
        iman)
		tomcat
        ;;
        nagios)
		nagios
        ;;
	noc)
		noc
	;;
	superlumin)
		superlumin
	;;
	server)
		server
	;;
        *)
        echo "Usage: $0 {edir|lag|lage|idp|consola|auth|ciec|wsau|wsfe|uapp|iman|nagios|noc|superlumin}"
        exit 1
        ;;
esac
